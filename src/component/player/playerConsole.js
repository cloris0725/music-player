import React, {Component} from "react";
import "./player.css"
import PlayCircle from "../../../src/img/PlayCircle.svg"
import Menu from "../../../src/img/Menu.svg"
import Share from "../../../src/img/Share.svg"

class PlayerConsole extends Component{
    render() {
        return(
            <div className="topRight">
                <div className="topRightWrapper">
                    <p>PLAYLIST</p>
                    <div className="list">Summer-Mood-List</div>
                    <div className="btn">
                        <ul>
                            <li><a href=""><img src={PlayCircle} alt=""/></a></li>
                            <li><a href=""><img src={Share} alt=""/></a></li>
                            <li><a href=""><img src={Menu} alt=""/></a></li>
                        </ul>
                        <div className="listDescription">
                            7 songs (28 minutes)
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


export default PlayerConsole