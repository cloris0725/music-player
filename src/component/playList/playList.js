import React, {Component} from "react";
import SongItem from "./songItem";
import "./playList.css"
import {connect} from "react-redux";
import fetchAllSongs from "../../action/songAction";
import PlayCircle from "../../../src/img/PlayCircle.svg"



class PlayList extends Component{
    //
    // state= {
    //     background: "pink"
    // }
    //
    // setBackground = () => {
    //     const {background} = this.state
    //
    // }


    render() {
        return (
            <div className="playList">
                <SongItem songs={this.props.songs} ></SongItem>
            </div>
        );
    }

    componentDidMount() {
        this.props.fetchAllSongs()
    }



}

// class ShowIcon extends Component{
//     render() {
//         return(
//             <div><img src={PlayCircle} width="40" alt=""/></div>
//         )
//     }
// }
//component需要和reducer建立联系：用mapStateToProps确定用哪一个reducer，再使用connect（mapStateToProps）（要传入的component）中两个参数连接

const mapStateToProps = (state) => {
    return{
        songs: state.songReducer
    }

}


export default connect(mapStateToProps,{fetchAllSongs})(PlayList)
