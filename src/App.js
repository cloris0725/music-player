import React from 'react';
import './App.css';
import Player from "./component/player/player";
import PlayList from "./component/playList/playList";

function App() {
  return (
    <div className="player">
        <div className="playerContainer">
            <Player></Player>
            <PlayList></PlayList>
        </div>


    </div>
  );
}

export default App;
