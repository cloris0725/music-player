import React, {Component} from "react";
import "./playList.css"
import Cute from "../../../src/img/Cute.jpg"




class SongItem extends Component{
    constructor() {
        super();
        this.state = {horveredSongId: -1, isClicked: true, clickId: null}

    }

    chkboxClick() {
        let chkboxArray = document.getElementsByClassName('Input')
        // console.log('show--->', chkboxArray)
        // let test = document.getElementById('test')
        let ifClicked = false
        for (let i = 0; i < chkboxArray.length; i++){
            if (chkboxArray[i].checked){

                ifClicked = true
                break
            }else{
                ifClicked = false
            }
            console.log('show--->', chkboxArray[i].checked)
        }
        if (ifClicked === true){
            for (let i = 0; i < chkboxArray.length; i++){
                chkboxArray[i].style.visibility = "visible"
            }
        }else{
            for (let i =0; i < chkboxArray.length; i++){
                chkboxArray[i].style.visibility = ""
            }
        }


    }



    showMenu(id) {
        this.setState({isClicked: false, clickId: id})
    }

    // hideMenu(id){
    //     this.setState({isClicked: true, clickId: id})
    // }


    // hideMenu() {
    //     let menuDiv = document.getElementsByClassName("dropdownMenu")
    //     for (let i = 0; i < menuDiv.length; i++){
    //         menuDiv[i].style.display = "none"
    //         // menuDiv[i].style.visibility = "hidden"
    //     }
    // }
    //
    //
    // showMenu() {
    //     let menuListArray = document.getElementsByClassName("menuList")
    //     console.log('menuListArray--->',menuListArray )
    //     for (let i = 0; i < menuListArray.length; i++){
    //         if (menuListArray[i].onclick){
    //             menuListArray[i].style.display = "flex"
    //         }
    //
    //     }
    // }
    //
    showArrowBox = () => {
        return (
            <div className="dropdownMenu" >
                <ul id="arrowMenu">
                <li className="menuList">Share song</li>
                <li className="menuList">Add to playlist</li>
                <li className="menuList">Remove from playlist</li>
                <li className="menuList">View album</li>
                </ul>
            </div>
        )
    }

    // toggleDiv = (sid) => {
    //     const {hideAllCheck} = this.state
    //     const ssArr = this.state.selectedSongs
    //     if(ssArr.some(ss => ss.id === sid)) {
    //         console.log(`s, sid => ${sid}`)
    //         this.setState({selectedSongs: [...ssArr, {[sid]: !ssArr[sid]}]})
    //     } else {
    //         this.setState({selectedSongs: [...ssArr, {[sid]: true}]})
    //     }
    //
    //     this.setState({hideAllCheck: !hideAllCheck})
    // }

    render() {
        return this.props.songs.map(song => {
                let hovered = this.state.hoveredSongId === song.id
                let classHighlight = 'songItem'
                if (hovered){
                    classHighlight = 'selected'
                }

            const alternativeView = hv => {return {visibility: hv? 'hidden' : 'visible'}}
            const alternativeDisplay = hv => {return {display: hv? 'flex' : 'none'}}


            return(<div className={classHighlight} key={song.id}
                   onMouseOver={_ => this.setState({hoveredSongId: song.id})}
                        onMouseEnter={this.hideMenu}
                onMouseLeave={_ => this.setState({hoveredSongId: -1})}
                >
                    <div className="chkbox"
                         style={alternativeView(!hovered)}>
                        <input className="Input" type="checkbox" id="test"
                               onClick={this.chkboxClick}/>

                        {/*{console.log(this.state.hideAllCheck)}*/}
                    </div>
                    
                    <div className="btn"
                         style={alternativeDisplay(!hovered)}>
                        <div className="row-2">{song.id}</div>
                    </div>
                    <div className="btn-2"
                         style={alternativeDisplay(hovered)}>
                        <i className="far fa-play-circle"></i>
                    </div>
                    <div className="img"><img className="row-4" src={Cute} width="30" alt="#"/></div>
                    <div className="songDetails"><div className="div1">{song.name}</div><div className="div2">{song.artist}</div></div>
                    <div style={alternativeDisplay(hovered)}> {this.state.clickId === song.id? this.showArrowBox(): " "}</div>
                    <div className="songLength" style={alternativeDisplay(!hovered)} >
                       <div className="row-6">{song.length}</div>
                    </div>
                    <div className="more"
                         style={alternativeDisplay(hovered)}>
                        <i className="fas fa-ellipsis-v"
                           onClick={event => this.showMenu(song.id)}
                           // onMouseLeave={event => this.hideMenu(song.id)}
                        ></i>



                    </div>
                </div>
            )
        }

        )
    }
}


export default SongItem



