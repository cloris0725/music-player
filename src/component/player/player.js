import React, {Component} from "react";
import Cover from "./cover";
import PlayerConsole from "./playerConsole";
import "./player.css"


class Player extends Component{
    render() {
        return(
            <div className="topContainer">
                <Cover></Cover>
                <PlayerConsole></PlayerConsole>
            </div>
        )
    }
}

export default Player




