
import {FETCH_ALL_SONGS} from "../helper";
import {combineReducers} from "redux";

const initState = []

const songReducer = (state = initState, action) => {
    console.log(action.payload)
    switch (action.type){
        case FETCH_ALL_SONGS:
            console.log("hi",action.payload)
            return action.payload

        default:
            return state
    }
}

export default combineReducers(
    {
        songReducer
    }
)

