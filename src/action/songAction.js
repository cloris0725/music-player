import {FETCH_ALL_SONGS} from "../helper";

const fetchAllSongs = () => {
    return {
        type: FETCH_ALL_SONGS,
        payload: [
            {id:1, name: "Tired", artist:"Vietra", length: 1.24, selected:false},
            {id:2, name: "Quelqu'un M'a Dit", artist:"Carla Bruni", length:3.34, selected: false},
            {id:3, name: "Loving Strangers", artist:"Jocelyn Pook/Russian Red", length:3.34, selected: false},
            {id:4, name: "la vie en rose", artist:"Olivia Herdt", length:3.34, selected: false},
            {id:5, name: "Lucky Me", artist:"Craig Reever", length:3.34, selected: false},
            {id:6, name: "Claire De Lune", artist:"Achille-Claude Debussy", length:3.34, selected: false},
            {id:7, name: "Gymnopedie No.1", artist:"Martin Ermen", length:3.34, selected: false},

        ]
    }
}

export default fetchAllSongs